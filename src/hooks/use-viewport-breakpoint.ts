import { useEffect, useState } from 'react'

type ViewportBreakpoint = 'xs' | 'md' | 'lg' | 'xl'

interface ViewportBreakpointConfig {
  size: ViewportBreakpoint
  mql: MediaQueryList
}

export const useViewportBreakpoint = () => {
  const [viewportBreakpoint, setViewportBreakpoint] =
    useState<ViewportBreakpoint>('xs')

  useEffect(() => {
    const mqls: ViewportBreakpointConfig[] = [
      { size: 'xs', mql: window.matchMedia('(max-width: 767px)') },
      {
        size: 'md',
        mql: window.matchMedia('(min-width: 768px) and (max-width: 1439px)'),
      },
      {
        size: 'lg',
        mql: window.matchMedia('(min-width: 1440px) and (max-width: 1919px)'),
      },
      { size: 'xl', mql: window.matchMedia('(min-width: 1920px)') },
    ]

    const checkMatch = () =>
      mqls.forEach(
        ({ size, mql }) => mql.matches && setViewportBreakpoint(size),
      )

    mqls.forEach(({ mql }) => mql.addEventListener('change', checkMatch))
    checkMatch()

    return () => {
      mqls.forEach(({ mql }) => mql.removeEventListener('change', checkMatch))
    }
  }, [])

  return viewportBreakpoint
}
