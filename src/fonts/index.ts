import localFont from 'next/font/local'
import {Montserrat} from "next/font/google";

export const suboleya = localFont({ src: '../../public/fonts/Suboleya.ttf' })
export const bowler = localFont({ src: '../../public/fonts/BOWLER.otf' })
export const montserrat = Montserrat({
    weight: ['500', '600'],
    subsets: ['latin'],
    display:'swap',
    fallback: ['Arial', 'sans-serif'],
});
