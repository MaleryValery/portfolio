import {BlogFilter} from "@/components/Blog-filter/Blog-filter";
import {BlogList} from "@/components/Blog-list/Blog-list";

export default function Blog() {

    return (
        <div className='mt-[76px] mb-0 relative pt-4 md:flex'>
            <div className='md:w-1/4 min-w-[300px] md:mt-4'>
                <BlogFilter/>
            </div>
            <BlogList/>
        </div>
    )
}
