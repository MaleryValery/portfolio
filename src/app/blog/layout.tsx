import {ReactNode} from "react";

export default function blogLayout({children}: {children: ReactNode}) {
    return (
        <section className='bg-[#F1F1F1] h-svh overflow-y-auto'>
            {children}
        </section>
    )
}