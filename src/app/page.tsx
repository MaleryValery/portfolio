import {Footer, Hero, SectionAbout, SectionContent, SectionServices,} from '@/components'

export default function Home() {
  return (
    <>
      <Hero />
      <main>
        <SectionAbout />
        <SectionServices />
        <SectionContent />
      </main>
      <Footer />
    </>
  )
}
