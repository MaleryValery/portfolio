import React, {ComponentPropsWithoutRef, forwardRef} from 'react'
import '@/components/Card/Card.scss'
import {cn} from "@/lib/utils";

type CardProps = {
    variant?: 'blog' | 'abouts' | 'service' | 'content' | 'blogsFilter'
    children?: React.ReactNode
} & ComponentPropsWithoutRef<'div'>

export const Card = forwardRef<HTMLDivElement, CardProps>((props, ref) => {
    const {className, variant, children, ...restProps} = props
    const cardStyles = cn(`card card-${variant}`, className)
    const stick = cn(`relative whitespace-pre-wrap pl-[17px] before:absolute before:left-[5px] before:top-[5%] before:h-[90%] before:w-[2px] before:bg-[#838DD1] before:hover:bg-stick before:hover:shadow-stick before:transition-shadow before:duration-300`,
        variant === 'abouts' && 'pl-[27px] before:left-0 before:top-0 before:h-full')

    return <div ref={ref} className={cardStyles} {...restProps}>
        <div
            className={(variant === 'abouts' || variant === 'blog') ? stick : ''}
        >{children}</div>
    </div>
})
