import LeftArrow from '@/../public/icons/angle-left.svg'
import RightArrow from '@/../public/icons/angle-right.svg'
import Image from "next/image";
import {cn} from "@/lib/utils";
import {DOTS, usePagination} from "@/components";

type PaginationProps = {
    onPageChange: (nextPage: number) => void
    totalCount: number
    siblingCount?: number
    currentPage: number
    pageSize?: number
}
const arrowClass = 'flex justify-center bg-white w-6 h-6 select-none rounded border-[1px] border-[#40446F] active:bg-[#40446F]'
const numbersClass = `flex justify-center items-center bg-white text-[#A3B0E0] w-6 h-6 border-[1px] border-[#838DD1] select-none rounded`

export const Pagination = ({
                               onPageChange,
                               totalCount,
                               siblingCount = 1,
                               currentPage,
                               pageSize = 10,
                           }: PaginationProps) => {

    const paginationRange = usePagination({currentPage, totalCount, siblingCount, pageSize})
    if (currentPage === 0 || !paginationRange || paginationRange.length < 2) {
        return null
    }
    const nextPageButtonHandler = () => {onPageChange(currentPage + 1)}
    const previousPageButtonHandler = () => {onPageChange(currentPage - 1)}
    const lastPage = paginationRange[paginationRange.length - 1]

    return (
        <div className="flex gap-2 justify-center">
            <li
                className={cn(arrowClass, currentPage === 1 ? 'bg-[#40446F] border-[1px] border-[#838DD1] pointer-events-none' : 'bg-[#838DD1] cursor-pointer hover:bg-[#a8afdf]')}
                onClick={previousPageButtonHandler}>
                <Image src={LeftArrow} alt='prev page' width={15}/>
            </li>
            {paginationRange.map((pageNumber, idx) => {
                if (pageNumber === DOTS) {
                    return (
                        <li key={idx} className={numbersClass}>
                            ...
                        </li>
                    )
                }
                return (
                    <li
                        className={cn(numbersClass, pageNumber === currentPage ? 'border-[#40446F] bg-[#cdd1ed]' : 'cursor-pointer hover:bg-gray-300')}
                        onClick={() => onPageChange(+pageNumber)}
                        key={idx}>
                        {pageNumber}
                    </li>
                )
            })}
            <li
                className={cn(arrowClass, currentPage === lastPage ? 'bg-[#40446F] border-[1px] border-[#838DD1] pointer-events-none' : 'bg-[#838DD1] cursor-pointer hover:bg-[#a8afdf]')}
                onClick={nextPageButtonHandler}>
                <Image src={RightArrow} alt='next page' width={15}/>
            </li>
        </div>
    )
}



