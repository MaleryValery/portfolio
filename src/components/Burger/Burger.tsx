import { forwardRef } from 'react'
import { cn } from '@/lib/utils'

type BurgerProps = {
  opened: boolean
} & React.ComponentPropsWithoutRef<'button'>

export const Burger = forwardRef<HTMLButtonElement, BurgerProps>(
  (props, ref) => {
    const { opened, className, ...others } = props

    return (
      <button
        ref={ref}
        {...others}
        className={cn('flex h-6 w-6 items-center justify-center', className)}
      >
        <div data-opened={opened} className="burger" />
      </button>
    )
  },
)
