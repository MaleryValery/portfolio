import Image from 'next/image'
import { Logo } from '@/components'

export function Footer() {
  return (
    <footer className="flex justify-between bg-[#2F323E] p-5 lg:px-[143px] xl:pl-[248px] xl:pr-[260px]">
      <Logo className="md:text-4xl/normal lg:text-5xl/[1.3]" />
      <div className="flex gap-[33px] text-base/normal md:gap-[50px]">
        <div className="hidden md:block">
          <h3 className="mb-[16px] text-[#838DD1]">Карта сайта</h3>
          <nav className="flex flex-col items-center gap-3 md:items-stretch">
            <a href="/">Главная</a>
            <a href="/services">Услуги</a>
            <a href="/content">Контент</a>
          </nav>
        </div>
        <div className="">
          <h3 className="mb-[10px] text-[#838DD1] md:mb-[16px]">
            Мы в соцсетях
          </h3>
          <nav className="flex flex-col items-center gap-3 md:items-stretch">
            <a href="/#">
              <Image
                width={30}
                height={21}
                src="/icons/yt.svg"
                alt="youtube"
                className="md:hidden"
              />
              <span className="hidden md:block">Youtube канал</span>
            </a>
            <a href="/#">
              <Image
                width={30}
                height={21}
                src="/icons/yt.svg"
                alt="youtube"
                className="md:hidden"
              />
              <span className="hidden md:block">Youtube канал</span>
            </a>
            <a href="/#">
              <Image
                width={24}
                height={24}
                src="/icons/vk.svg"
                alt="youtube"
                className="md:hidden"
              />
              <span className="hidden md:block">VK</span>
            </a>
          </nav>
        </div>
        <div>
          <h3 className="mb-[10px] text-[#838DD1] md:mb-[16px]">
            Мы в Telegram
          </h3>
          <nav className="flex flex-col items-center gap-2.5 md:items-stretch md:gap-3">
            <a href="/#">
              <Image
                width={25}
                height={24}
                src="/icons/tg.svg"
                alt="youtube"
                className="md:hidden"
              />
              <span className="hidden md:block">@JunovNet</span>
            </a>
            <a href="/#">
              <Image
                width={25}
                height={24}
                src="/icons/tg.svg"
                alt="youtube"
                className="md:hidden"
              />
              <span className="hidden md:block">@Django School</span>
            </a>
            <a href="/#">
              <Image
                width={25}
                height={24}
                src="/icons/tg.svg"
                alt="youtube"
                className="md:hidden"
              />
              <span className="hidden md:block">@JunovNet Community</span>
            </a>
          </nav>
        </div>
      </div>
    </footer>
  )
}
