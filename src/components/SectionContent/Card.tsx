import Image from 'next/image'
import Link from 'next/link'

interface CardProps {
  type: 'yt' | 'tg' | 'vk'
  title?: string
  label: string
  href: string
}

export function Card(props: CardProps) {
  const { label, href, type, title = 'Django School' } = props

  const options = {
    yt: {
      linkTitle: 'Youtube',
      image: <Image width={50} height={33} src="/icons/yt.svg" alt="yt" />,
    },
    tg: {
      linkTitle: 'Telegram',
      image: <Image width={35} height={35} src="/icons/tg.svg" alt="tg" />,
    },
    vk: {
      linkTitle: 'VK',
      image: <Image width={35} height={35} src="/icons/vk.svg" alt="vk" />,
    },
  }

  const { image, linkTitle } = options[type]

  return (
    <div className="flex max-w-[351px] flex-col rounded-sm border-2 border-[#838DD1] bg-[#2B3142CC] px-[5px] pb-[17px] pt-[9px] backdrop-blur-[7.5px] hover:shadow-stick">
      <p className="flex items-center gap-5 text-3xl/normal">
        {image}
        {title}
      </p>
      <p className="mb-5 whitespace-pre-line text-2xl/normal">{label}</p>
      <Link href={href} className="btn btn-base btn-slate mr-[15px] self-end">
        Перейти в {linkTitle}
      </Link>
    </div>
  )
}
