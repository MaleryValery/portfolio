'use client'
import { H2 } from '@/components'
import { useViewportBreakpoint } from '@/hooks/use-viewport-breakpoint'
import { Card } from './Card'

const contents = {
  tg: [
    {
      id: 0,
      label: `Все о создании сайтов, web-app и программировании. Архитектура ПО и инструменты.\n\nPython,Django, Starlette, FastAPI, Masonite и многое другое.`,
      href: '/',
    },
    {
      id: 1,
      label: `Все о создании сайтов, web-app и программировании. Архитектура ПО и инструменты.\n\nPython,Django, Starlette, FastAPI, Masonite и многое другое.`,
      href: '/',
    },
    {
      id: 2,
      label: `Все о создании сайтов, web-app и программировании. Архитектура ПО и инструменты.\n\nPython,Django, Starlette, FastAPI, Masonite и многое другое.`,
      href: '/',
    },
  ],
  yt: [
    {
      id: 0,
      label: `Все о создании сайтов, web-app и программировании. Архитектура ПО и инструменты.\n\nPython,Django, Starlette, FastAPI, Masonite и многое другое.`,
      href: '/',
    },
    {
      id: 1,
      label: `Все о создании сайтов, web-app и программировании. Архитектура ПО и инструменты.\n\nPython,Django, Starlette, FastAPI, Masonite и многое другое.`,
      href: '/',
    },
  ],
  vk: [
    {
      id: 0,
      label: `Все о создании сайтов, web-app и программировании. Архитектура ПО и инструменты.\n\nPython,Django, Starlette, FastAPI, Masonite и многое другое.`,
      href: '/',
    },
  ],
}

export function SectionContent() {
  const mediaQuery = useViewportBreakpoint()

  return (
    <section className="bg-[#1E1E1E] pb-[112px] pt-[50px] md:pb-[86px] md:pt-[100px] lg:pb-[116px] xl:pb-[210px]">
      <H2 className="mb-[50px] pl-[50px] md:mb-[100px] xl:pl-[100px]">
        Контент
      </H2>
      <div className="grid items-center justify-center gap-x-5 gap-y-[50px] px-[5px] md:gap-x-[27px] md:px-5 lg:grid-cols-[repeat(3,351px)] lg:place-items-center lg:gap-x-[95px] xl:grid-cols-[repeat(4,351px)] xl:gap-x-[100px]">
        {['xs', 'md'].includes(mediaQuery) && (
          <>
            <div className="flex flex-col gap-5 md:grid md:grid-cols-2 md:gap-x-[27px] lg:grid-cols-3">
              {contents.tg.map(({ label, href, id }) => {
                return <Card key={id} href={href} label={label} type="tg" />
              })}
            </div>
            <div className="flex flex-col gap-5 md:grid md:grid-cols-2 md:gap-x-[27px] lg:grid-cols-3">
              {contents.yt.map(({ label, href, id }) => {
                return <Card key={id} href={href} label={label} type="yt" />
              })}
            </div>
            <div className="flex flex-col gap-5 md:grid md:grid-cols-2 md:gap-x-[27px] lg:grid-cols-3">
              {contents.vk.map(({ label, href, id }) => {
                return <Card key={id} href={href} label={label} type="vk" />
              })}
            </div>
          </>
        )}
        {['lg', 'xl'].includes(mediaQuery) && (
          <>
            {contents.tg.map(({ label, href, id }) => {
              return <Card key={id} href={href} label={label} type="tg" />
            })}
            {contents.yt.map(({ label, href, id }) => {
              return <Card key={id} href={href} label={label} type="yt" />
            })}
            {contents.vk.map(({ label, href, id }) => {
              return <Card key={id} href={href} label={label} type="vk" />
            })}
          </>
        )}
      </div>
    </section>
  )
}
