import {Card} from "@/components/Card/Card";
import {Badge} from "@/components/Badge/Badge";
import Image from 'next/image'
import message from "@/../public/icons/message.svg"
import eye from "@/../public/icons/eye.svg"
import {cn} from "@/lib/utils";
import {Post} from "@/components/const";
import {montserrat} from "@/fonts";

interface Props {
    className?: string
    blogData: Post
}

export function BlogCard ({className, blogData}: Props) {
    const {badges, description, title, viewers, user, userAvatar, comments} = blogData
    return (
        <Card variant='blog' className={cn('relative ', className)}>
            <Image
                className='absolute right-0 top-0 select-none'
                src={`/images/${badges[0]}.svg`}
                width={150}
                height={150}
                alt='mainTechnology' />
            <div className='flex w-full justify-between pt-3'>
                <span className="font-semibold text-lg">{title}</span>
                <span className='text-xs font-semibold pr-[3%]'>04.10.2023</span>
            </div>
            <p className={`text-sm font-medium ${montserrat.className} pr-[5%]`}>{description}</p>
            <div className='flex gap-2 pt-[5px]'>
                {badges.map((badge, index) => {
                    const badgeColors: ("blue" | "green" | "red")[] = ['blue', 'green', 'red'];
                    const color = badgeColors[index] || 'red';
                    return (
                        <Badge key={index} size="sm" color={color}>
                            <span className={`text-xs font-semibold ${montserrat.className} `}>{badge}</span>
                        </Badge>
                    );
                })}
            </div>
            <div className='flex justify-between pr-[5px] pb-3'>
                <div className='flex gap-4 py-[5px]'>
                    <img className='w-[30px] rounded-2xl' src={userAvatar} alt="ava"/>
                    <span className='flex flex-col justify-center text-xs font-semibold'>{user}</span>
                </div>
                <div className="flex gap-3 pr-[3%]">
                    <span className="flex items-center gap-1">
                        <Image src={message} alt='comments'/>
                        <span className='text-xs font-semibold'>{comments}</span>
                    </span>
                    <span className="flex items-center gap-1">
                        <Image src={eye} alt='viewers'/>
                        <span className='text-xs font-semibold'>{viewers}</span>
                    </span>
                </div>
            </div>
        </Card>
    )
}