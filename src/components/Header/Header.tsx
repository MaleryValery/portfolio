'use client'

import clsx from 'clsx'
import Image from 'next/image'
import Link from 'next/link'
import { Burger, Logo, Navs } from '@/components'
import routes from '@/data/routes.json'
import { useDisclosure } from '@/hooks/use-disclosure'

export function Header() {
  const [opened, { toggle }] = useDisclosure()

  return (
    <header className="fixed left-0 top-0 z-50 flex w-full items-center justify-between bg-[#2C3449] p-5 shadow-md backdrop-blur-[10px] md:px-[50px] md:pb-[11px] md:pt-3">
      <Logo />
      <Navs routes={routes} className="hidden md:flex" />
      <Burger
        opened={opened}
        onClick={toggle}
        className="relative z-50 md:hidden"
      />
      <div
        className={clsx(
          !opened && 'hidden',
          'absolute bottom-0 left-0 right-0 top-0 z-40 flex h-screen flex-col gap-[30px] overflow-y-scroll bg-[#2C3449] px-4 pt-[120px] text-xl/normal',
        )}
      >
        <div className="flex flex-col items-center gap-5">
          <Image width={40} height={40} src="/icons/avatar.svg" alt="avatar" />
          Гость
        </div>
        <nav className="mb-[76px] flex flex-col gap-[30px]">
          {routes.map(({ href, id, title }) => (
            <Link key={id} href={href}>
              {title}
            </Link>
          ))}
        </nav>
        <div className="flex justify-center gap-8 px-8 opacity-70">
          <Link href="/">
            <Image width={24} height={24} src="/icons/vk.svg" alt="vk" />
          </Link>
          <Link href="/">
            <Image
              width={25}
              height={24}
              src="/icons/telegram.svg"
              alt="telegram"
            />
          </Link>
          <Link href="/">
            <Image width={30} height={21} src="/icons/yt.svg" alt="youtube" />
          </Link>
        </div>
      </div>
    </header>
  )
}
