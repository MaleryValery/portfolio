import Link from 'next/link'
import { cn } from '@/lib/utils'

interface LogoProps {
  className?: string
}

export function Logo({ className }: LogoProps) {
  return (
    <Link
      href="/"
      className={cn('text-2xl/normal md:text-5xl/[1.3]', className)}
    >
      Logo
    </Link>
  )
}
