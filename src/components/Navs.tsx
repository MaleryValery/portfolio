import Image from 'next/image'
import Link from 'next/link'
import { cn } from '@/lib/utils'

interface NavsProp {
  className?: string
  routes: {
    id: number
    href: string
    title: string
  }[]
}

export function Navs({ routes, className }: NavsProp) {
  return (
    <nav
      className={cn(
        'flex items-center justify-between gap-5 text-xl/normal',
        className,
      )}
    >
      {routes.map(({ href, id, title }) => (
        <Link key={id} href={href}>
          {title}
        </Link>
      ))}
      <Image width={40} height={40} src="/icons/avatar.svg" alt="avatar" />
    </nav>
  )
}
