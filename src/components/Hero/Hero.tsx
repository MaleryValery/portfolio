import Image from 'next/image'
import Link from 'next/link'
import { Button } from '@/components'
import { bowler } from '@/fonts'
import { cn } from '@/lib/utils'

export function Hero() {
  return (
    <div className="mt-[76px] bg-[#2C3449] px-5 pb-[196px] pt-[50px] md:mt-[85px] md:px-[50px] md:pb-[411px] md:pt-[100px]">
      <div className="mb-[50px] w-[37ch] md:w-[69ch] lg:mb-[150px] lg:w-[88ch] xl:w-[107ch]">
        <h1
          className={cn(
            bowler.className,
            'text-lg/normal md:text-3xl/normal lg:text-5xl/normal xl:text-6xl/normal',
          )}
        >
          Временный текст тут будет другое
        </h1>
        <p className="text-base/normal md:text-2xl/normal xl:text-3xl/normal">
          Привет, Как дела? Деньги давай, без этого никак броу. Дальше лан
          рандом текст. Я гениратор текста. Мне с кайфом учится тут можно без
          проблем.
        </p>
      </div>
      <div className="space-y-[14px] md:space-y-[17px]">
        <Link href="#">
          <Button
            className="text-[10px] md:text-lg/normal"
            leftSection={
              <Image
                width={24}
                height={24}
                src="/icons/telegram.svg"
                alt="telegram"
                className="h-4 w-4 md:h-6 md:w-6"
              />
            }
          >
            Связаться в Telegram
          </Button>
        </Link>
        <div className="w-[23ch] space-y-[10px] md:w-[33ch]">
          <p className="text-xs/normal md:text-sm/normal">
            Я использую Telegram, чтобы общаться и клиентами, но вы можете
            связаться со мной в Discord, VK.
          </p>
          <div className="flex gap-5">
            <Link href="#">
              <Image
                width={24}
                height={24}
                src="/icons/discord.svg"
                alt="discord"
              />
            </Link>
            <Link href="#">
              <Image width={24} height={24} src="/icons/vk.svg" alt="vk" />
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}
