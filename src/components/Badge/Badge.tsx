import React, {ComponentPropsWithoutRef, forwardRef, ReactNode} from "react";
import {cn} from "@/lib/utils";
import './badge.scss'

type BadgeProps = {
    size?: 'sm' | 'base' | 'lg'
    color?: 'blue' | 'red' | 'green'
    Icon?: ReactNode
    children: ReactNode
} & ComponentPropsWithoutRef<'span'>
export const Badge = forwardRef<HTMLSpanElement, BadgeProps>((props, ref) => {
    const {children, size ='base', color, className, Icon, ...restProps} = props
    return(
        <span ref={ref} className={cn(`badge badge-${size} badge-${color}`, className)} {...restProps}>
            {Icon && <span>{Icon}</span>}
            {children}
        </span>
    )
})