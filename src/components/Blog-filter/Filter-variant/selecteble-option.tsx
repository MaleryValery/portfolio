import React, {ReactNode, useState} from "react";
import {cn} from "@/lib/utils";

interface Props {
    children: ReactNode
    active?: boolean
}
export function SelectebleOption ({children}: Props) {
    const [active,setActive] = useState(false)

    return (
        <div onClick={()=>setActive(!active)} className={cn('select-none cursor-pointer relative whitespace-pre-wrap pl-3 text-black before:absolute before:left-[5px] before:top-[5%] before:h-[90%] before:w-[2px] before:bg-[#838DD1] before:hover:bg-stick before:hover:shadow-stick before:transition-shadow before:duration-300', !active && 'before:hidden' )}
        >{children}</div>
    )
}