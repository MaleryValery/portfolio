"use client"
import {Card} from "@/components/Card/Card";
import {Input} from "@/components/Input/Input";
import {Button} from "@/components";
import {useState} from "react";
import {SelectebleOption} from "@/components/Blog-filter/Filter-variant/selecteble-option";
import {FilterValues} from "@/components/const";
import {cn} from "@/lib/utils";
import {montserrat} from "@/fonts";

export function BlogFilter() {
    const [show, setShow] = useState<boolean>(true)
    return (<>
        <div
            onClick={() => setShow(!show)}
            className={`select-none absolute left-[46%] top-[0] bg-[#2C3449] px-4 cursor-pointer md:hidden font-semibold ${montserrat.className}`}
        > Filter
        </div>

        <Card variant='blogsFilter' className={cn(`md:flex md:flex-col`, show && 'hidden')}>
            <div className='flex gap-4 justify-center pb-4'>
                <Input
                    sizing='sm'
                    placeholder='Поиск материала'/>
                <Button className='h-[30px] w-[60px]'>Поиск</Button>
            </div>
            <div className='flex md:flex-col flex-wrap gap-2 md:h-[calc(100vh-300px)] md:overflow-y-hidden'>
                {FilterValues.map((el, index) => {
                    return <SelectebleOption key={index}>{el}</SelectebleOption>
                })}
            </div>
        </Card>
    </>)
}