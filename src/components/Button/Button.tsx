import { forwardRef } from 'react'
import { cn } from '@/lib/utils'

type ButtonProps = {
  leftSection?: React.ReactNode
  rightSection?: React.ReactNode
} & React.ComponentPropsWithoutRef<'button'>

export const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (props, ref) => {
    const { children, leftSection, rightSection, className, ...others } = props

    return (
      <button
        ref={ref}
        className={cn(`btn btn-xs btn-slate`, className)}
        {...others}
      >
        {leftSection && <span>{leftSection}</span>}
        {children}
        {rightSection && <span>{rightSection}</span>}
      </button>
    )
  },
)
