'use client'
import React, {useEffect, useState} from 'react';
import {BlogCard} from "@/components/Blog-card/blog-card";
import {Pagination} from "@/components/Pagination/Pagination";
import {blogPost, Post, POSTS_PER_PAGE} from "@/components/const";

export const BlogList = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [currentPosts, setCurrentPosts] = useState<Post[]>([])

    useEffect(() => {
        const indexOfLastPost = currentPage * POSTS_PER_PAGE;
        const indexOfFirstPost = indexOfLastPost - POSTS_PER_PAGE;
        const slicedPosts = blogPost.slice(indexOfFirstPost, indexOfLastPost);
        setCurrentPosts(slicedPosts);
    }, [currentPage])

    const handlePageChange = (pageNumber: number) => {
        setCurrentPage(pageNumber);
    }
    return (
        <div className='flex flex-col gap-4 py-4 md:w-3/4 max-w-[900px]'>
            {currentPosts.map((data) => <BlogCard key={data.id} className='sm:w-3/4' blogData={data}/>)}
            <Pagination
                onPageChange={handlePageChange}
                totalCount={blogPost.length}
                currentPage={currentPage}
            />
        </div>
    );
};