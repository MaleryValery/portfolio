import {H2} from '@/components'
import {Card} from "@/components/Card/Card";

const abouts = [
  {
    id: 0,
    content: `Тут впишу какой нибудь глупый анекдот надеюсь это не будут читать.\n\nДагестанские блохи, услышав лезгинку, затоптали кошку насмерть. КОНЕЦ ТУТ УЖЕ ТОЧНО`,
  },
  {
    id: 1,
    content: `Мои работы вы не увидите, я их спалил.\nТеперь идёт фулл рандом текст. мне хочется сюда вписать больше текста. Машинка едет по алее и всё.\nДальше я не придумал. Спасибо, что прочитали. хихи.\nКОНЕЦ`,
  },
  {
    id: 2,
    content: `Тут впишу какой нибудь глупый анекдот надеюсь это не будут читать.\n\nДагестанские блохи, услышав лезгинку, затоптали кошку насмерть. КОНЕЦ ТУТ УЖЕ ТОЧНО`,
  },
  {
    id: 3,
    content: `Привет! Меня зовут Денчик. Я кто-то там.\nВ этой сфере уже 15 лет. И за это время собрал багаж опыта и знаний.\nСейчас мой кателок не варит не могу придумать что сюда написать точька.\n\nМожет чтото придумаю ещё интересного Удачи`,
  },
]

export function SectionAbout() {
  return (
    <section className="bg-[#26272c] px-[1px] pb-[87px] pt-[50px] md:pb-[86px] md:pt-[100px] lg:pb-[150px] xl:pb-[220px]">
      <H2 className="mb-[50px] pl-5 md:mb-[100px] md:pl-[50px] xl:pl-[100px]">
        Обо мне
      </H2>
      <div className="grid justify-center gap-5 md:gap-[50px] md:px-5 lg:grid-cols-[repeat(2,632px)] lg:gap-x-[75px] lg:gap-y-[171px] lg:px-[50px] xl:gap-x-[356px] xl:gap-y-[262px] xl:px-[150px]">
        {abouts.map(({id, content}) => <Card variant='abouts' key={id} > {content} </Card>)}
      </div>
    </section>
  )
}
