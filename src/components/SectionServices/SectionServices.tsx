import { H2 } from '@/components'
import { Card } from './Card'

const services = [
  {
    id: 0,
    name: 'Kонсультации',
    currencyAmt: '3100',
    currencyType: 'руб',
    items: [
      'Бла бла бла что-то там гдето там',
      'Чо где а там',
      'Помощь надо',
      '13 42 помощьник ура ура',
      'ы где а тут ура захвзахв',
      'Спасибо что приобрели удачи оставаться',
      'Спасибо что приобрели удачи оставаться SUK MOUT SOWKS',
    ],
  },
  {
    id: 1,
    name: 'Kонсультации',
    currencyAmt: '3100',
    currencyType: 'руб',
    items: [
      'Бла бла бла что-то там гдето там',
      'Чо где а там',
      'Помощь надо',
      '13 42 помощьник ура ура',
      'ы где а тут ура захвзахв',
      'Спасибо что приобрели удачи оставаться',
      'Спасибо что приобрели удачи оставаться SUK MOUT SOWKS',
    ],
  },
  {
    id: 2,
    name: 'Kонсультации',
    currencyAmt: '3100',
    currencyType: 'руб',
    items: [
      'Бла бла бла что-то там гдето там',
      'Чо где а там',
      'Помощь надо',
      '13 42 помощьник ура ура',
      'ы где а тут ура захвзахв',
      'Спасибо что приобрели удачи оставаться',
      'Спасибо что приобрели удачи оставаться SUK MOUT SOWKS',
    ],
  },
  {
    id: 3,
    name: 'Kонсультации',
    currencyAmt: '3100',
    currencyType: 'руб',
    items: [
      'Бла бла бла что-то там гдето там',
      'Чо где а там',
      'Помощь надо',
      '13 42 помощьник ура ура',
      'ы где а тут ура захвзахв',
      'Спасибо что приобрели удачи оставаться',
      'Спасибо что приобрели удачи оставаться SUK MOUT SOWKS',
    ],
  },
]

export function SectionServices() {
  return (
    <section className="bg-[#262B2C] pb-[112px] pt-[50px] md:pb-[86px] md:pt-[100px] lg:pb-[116px] xl:pb-[210px]">
      <H2 className="mb-[50px] pl-5 md:mb-[100px] md:pl-[50px] xl:pl-[100px]">
        Услуги
      </H2>
      <div className="mx-[10px] grid grid-cols-[repeat(4,308px)] gap-5 overflow-auto md:mx-5 md:grid-cols-[repeat(2,350px)] md:justify-center md:gap-y-[50px] lg:grid-cols-[repeat(3,350px)] lg:gap-x-[94px] xl:grid-cols-[repeat(4,350px)] xl:gap-[70px]">
        {services.map(({ id, currencyAmt, currencyType, items, name }) => {
          return (
            <Card
              key={id}
              currencyAmt={currencyAmt}
              currencyType={currencyType}
              items={items}
              name={name}
            />
          )
        })}
      </div>
    </section>
  )
}
