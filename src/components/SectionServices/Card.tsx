import { Button } from '@/components'
import { bowler } from '@/fonts'
import { cn } from '@/lib/utils'

interface CardProps {
  name: string
  currencyAmt: string
  currencyType: string
  items: string[]
}

export function Card(props: CardProps) {
  const { currencyAmt, currencyType, items, name } = props

  return (
    <div className="flex max-w-[308px] flex-col justify-between  gap-5 rounded-md border border-[#838DD1] bg-[#2C2F38CC] py-5 md:max-w-[350px]">
      <h3 className={cn(bowler.className, 'text-center text-lg md:text-2xl')}>
        {name}
      </h3>
      <ul className="list-disc px-6 md:text-lg">
        {items.map((item) => {
          return <li key={item}>{item}</li>
        })}
      </ul>
      <div className="mx-5 flex items-center justify-between">
        <span className={cn(bowler.className, 'text-lg md:text-2xl')}>
          {currencyAmt}&nbsp;<sub>{currencyType}</sub>
        </span>
        <Button className="btn-xs md:btn-base">Описание</Button>
      </div>
    </div>
  )
}
