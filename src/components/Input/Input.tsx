"use client"

import {cn} from "@/lib/utils";
import eye from '@/../public/icons/eye.svg'
import eyeSlash from '@/../public/icons/eye-slash.svg'
import Image from 'next/image'
import {ComponentPropsWithoutRef, ReactNode, useState} from "react";
import './Input.scss'

type InputProps = {
    sizing?: 'sm' | 'base' | 'lg'
    leftSection?: ReactNode
    rightSection?: ReactNode
} & ComponentPropsWithoutRef<'input'>

export function Input (props:InputProps) {
    const {
        children,
        sizing = 'base',
        rightSection,
        className,
        type,
        ...others
    } = props

    const [isShown, setIsShown] = useState(false);

    return (
        <div className='relative'>
            <input
                className={cn(`input input-${sizing}`, className)}
                {...others}
            />
            <div className='absolute right-[5px] top-[3px]'>
                {rightSection && <span>{rightSection}</span>}
                {type === 'password' && (
                    <Image
                        onClick={() => setIsShown(!isShown)}
                        src={isShown ? eye : eyeSlash}
                        alt={isShown ? 'show' : 'hide'}
                    />
                )}
            </div>
        </div>
    )
}
