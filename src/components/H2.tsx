import { bowler } from '@/fonts'
import { cn } from '@/lib/utils'

interface H2Props {
  className?: string
  children: React.ReactNode
}

export function H2({ className, children }: H2Props) {
  return (
    <h2
      className={cn(
        bowler.className,
        'text-4xl/normal md:text-6xl/normal',
        className,
      )}
    >
      {children}
    </h2>
  )
}
