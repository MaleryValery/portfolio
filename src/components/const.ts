const ava = 'https://s3-alpha-sig.figma.com/img/7e1e/0985/85f3679b0ca0344260a36b24e981fa78?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=KOYswtM5Q-vSzRaFqnSYr7ctMOhbS3gW9tXNycifbLFvMCIpWFwbpU7NylPWFKTa2tDJvlHlD926vCV3ntv1rT0jnPxPW5B--VnbcTXfYAXTQ9Ou9zi~UJ6USYZ0rDUGTqKBMq9niJm21jRcPIbWrVz~DtTUMv9jc8pzH6TR3MyX-kAroDDeGbbSRpJ60LiEBpSo-9kyDUddX89UB-T1PzqFMysF5L6MoDEGH9oygxmysimwjrAVRnxCABORAzW7s3G4tmHWr1eYho4lntDWci33mxqLIPFSyFrUGXbusCOSeI6gj5nHuK8PRYIF19JPuXeaHaufI63xQ90djJ9A8Q__'

export interface Post {
    id:number
    title: string;
    badges: string[];
    description: string;
    user: string;
    userAvatar: string;
    comments: number;
    viewers: number;
}
const techWithImage = ['Python', 'Java', 'Django', 'atom', 'html', 'js']
export const FilterValues = ['Все', 'Python', 'Java', 'Django', 'Masonite', 'FastAPI', 'Pydantic', 'Celery', 'SQLAlchemy', 'TypeScript', 'Vue', 'JavaScript', 'Redis', 'Atom', 'Docker']
export const blogPost: Post[] = []
for (let i = 0; i <= 100; i++) {
    blogPost.push({
        id: i+1,
        title: 'Соображения высшего порядка',
        badges: [techWithImage[Math.floor(Math.random()*techWithImage.length)],FilterValues[Math.floor(Math.random()*FilterValues.length)],FilterValues[Math.floor(Math.random()*FilterValues.length)]],
        description: 'Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности играет важную роль в формировании всесторонне сбалансированных нововведений!',
        user: 'JohnDoe',
        userAvatar: ava,
        comments: Math.floor(Math.random()*100),
        viewers: Math.floor(Math.random()*100)
    })
}
export const POSTS_PER_PAGE = 10;
export const DOTS = '...'
