/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx,mdx}',],
  theme: {
    extend: {
      backgroundImage: {
        stick:
          'linear-gradient(0deg, rgba(255, 255, 255, 0.20) 0%, rgba(255, 255, 255, 0.20) 100%), #838DD1',
      },
      boxShadow: {
        stick:
          '0px 0px 5px 0px #909AE1, 0px 0px 10px 0px #909AE1, 0px 0px 15px 0px #909AE1, 0px 0px 5px 0px rgba(234, 236, 255, 0.10);',
        card: 'box-shadow: 0px 0px 5px 0px #7C87CF, 0px 0px 10px 0px #7C87CF, 0px 0px 15px 0px #7C87CF, 0px 0px 5px 0px rgba(234, 236, 255, 0.10);',
      },
      screens: {
        xs: {
          min: '360px',
        },
        md: {
          min: '768px',
        },
        lg: {
          min: '1440px',
        },
        xl: {
          min: '1920px',
        },
      },
    },
  },
  plugins: [],
}

