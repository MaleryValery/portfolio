const { configure, presets } = require('eslint-kit')

module.exports = configure({
  allowDebug: process.env.NODE_ENV !== 'production',
  presets: [
    presets.imports(),
    presets.prettier(),
    presets.typescript(),
    presets.react(),
    presets.nextJs(),
  ],
  extend: {
    root: true,
  },
})
